package br.com.todoapp.fragments.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.todoapp.R
import br.com.todoapp.data.models.Priority
import br.com.todoapp.data.models.ToDoData
import br.com.todoapp.databinding.RowLayoutBinding
import br.com.todoapp.fragments.list.ListFragmentDirections
import br.com.todoapp.fragments.list.adapter.ListAdapter.MyViewHolder

class ListAdapter: RecyclerView.Adapter<MyViewHolder>() {

    private var dataList = emptyList<ToDoData>()

    inner class MyViewHolder(private val viewBinding: RowLayoutBinding): RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(item: ToDoData){
            viewBinding.titleTxt.text = item.title
            viewBinding.descriptionTxt.text = item.description
            setupPriority(item.priority,viewBinding)

            viewBinding.rowBackground.setOnClickListener {
                val action = ListFragmentDirections.actionListFragmentToUpdateFragment(item)
                viewBinding.root.findNavController().navigate(action)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutBinding = RowLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(layoutBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    override fun getItemCount(): Int = dataList.size


    private fun setupPriority(priority: Priority,binding: RowLayoutBinding) {
        when(priority) {
            Priority.HIGH -> binding.priorityIndicator.setCardBackgroundColor(
                ContextCompat.getColor(binding.root.context, R.color.red))

            Priority.MEDIUM -> binding.priorityIndicator.setCardBackgroundColor(
                ContextCompat.getColor(binding.root.context, R.color.yellow))


            Priority.LOW -> binding.priorityIndicator.setCardBackgroundColor(
                ContextCompat.getColor(binding.root.context, R.color.green))

        }
    }

    fun setData(todosList: List<ToDoData>){
        val todoDiffUtil = ToDoDiffUtils(dataList,todosList)
        val toDoDiffResult = DiffUtil.calculateDiff(todoDiffUtil)
        dataList = todosList
        toDoDiffResult.dispatchUpdatesTo(this)
    }


    fun getAdapterItemPosition(viewHolder: RecyclerView.ViewHolder): ToDoData{
        return dataList[viewHolder.adapterPosition]
    }

    fun hasLeftTodosData(): Boolean{
        return dataList.isEmpty()
    }

}