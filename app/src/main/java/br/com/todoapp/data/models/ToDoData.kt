package br.com.todoapp.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import br.com.todoapp.data.models.Priority
import kotlinx.parcelize.Parcelize

@Entity(tableName = "todo_table")
@Parcelize
data class ToDoData(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var priority: Priority,
    var description: String
):Parcelable

/*
* Para o uso do atributo priority - que é do tipo  Priority - em conjunto com o banco de dados
* e o Room, será necessário criar um método de conversão. Isso se dá pelo fato do Room e
* consequentemente o banco de dados permitir somente o uso de tipos primitivos.
* Para realizar tais conversões, e necessário criar funções com a notação @TypeConverter, sendo
* que tais funções devem estar dentro de uma classe destinada a trabalhar elas.
* Também é necessário configurar a sua classe de Database para reconhecer os metodos de conversão, e
* para isso utilize a annotation @TypeConverters() e passe a classe que contém os métodos de
* conversão como parâmetro. Com isso, o Room saberá como converter automaticamente os tipos
* customizados (classes) para os tipos primitivos que o banco entende.
*
* */