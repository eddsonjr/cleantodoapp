package br.com.todoapp.data.models

enum class Priority {
    HIGH,
    MEDIUM,
    LOW
}