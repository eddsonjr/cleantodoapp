package br.com.todoapp.fragments.add

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import br.com.todoapp.R
import br.com.todoapp.data.models.ToDoData
import br.com.todoapp.data.viewmodel.ToDoViewModel
import br.com.todoapp.databinding.FragmentAddBinding
import br.com.todoapp.fragments.SharedViewModel

class AddFragment : Fragment() {

    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!

    private val todoViewModel: ToDoViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(layoutInflater)
        setHasOptionsMenu(true)
        setupPrioritySpinner()
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.add_fragment_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_add){
            insertDataToDb()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setupPrioritySpinner() {
        binding.spinnerPriority.onItemSelectedListener = sharedViewModel.spinnerListener
    }

    private fun insertDataToDb() {
        val mTitle = binding.todoNameEdit.text.toString()
        val mPriority = binding.spinnerPriority.selectedItem.toString()
        val mDescription = binding.todoDescriptionEdit.text.toString()
        val validation = sharedViewModel.verifyDataFromUser(mTitle,mDescription)

        if(validation){
            val newData = ToDoData(
                id = 0,
                title = mTitle,
                priority = sharedViewModel.parsePriority(mPriority),
                description = mDescription
            )
            todoViewModel.insertData(newData)
            Toast.makeText(
                requireContext(),
                getString(R.string.toast_insert_data_success),
                Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }else{
            Toast.makeText(
                requireContext(),
                getString(R.string.toast_fill_all_fields),
                Toast.LENGTH_SHORT).show()
        }
    }



    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}