package br.com.todoapp.fragments.list

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import br.com.todoapp.R
import br.com.todoapp.data.models.ToDoData
import br.com.todoapp.data.viewmodel.ToDoViewModel
import br.com.todoapp.databinding.FragmentListBinding
import br.com.todoapp.fragments.SharedViewModel
import br.com.todoapp.fragments.list.adapter.ListAdapter
import br.com.todoapp.fragments.list.adapter.SwipeToDelete
import br.com.todoapp.utils.observeOnce
import com.google.android.material.snackbar.Snackbar
import br.com.todoapp.utils.hideKeyboard


class ListFragment : Fragment(), SearchView.OnQueryTextListener {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    private val todoViewModel: ToDoViewModel by viewModels()
    private val shareViewModel: SharedViewModel by viewModels()
    private val adapter: ListAdapter by lazy { ListAdapter() }
    private lateinit var todoList: List<ToDoData>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(layoutInflater)
        goToAddFragment()
        setupAdapter()
        setupEmptyDataObserver()
        setHasOptionsMenu(true)
        hideKeyboard(requireActivity())
        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_fragment_menu,menu)
        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as SearchView
        searchView.isSubmitButtonEnabled = true
        searchView.setOnQueryTextListener(this)
        searchView.setOnCloseListener {
            adapter.setData(todoList)
            true

        }
    }


    private fun goToAddFragment() {
        binding.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_listFragment_to_addFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun setupEmptyDataObserver() {
        shareViewModel.emptyDatabase.observe(viewLifecycleOwner){ toShow ->
            showEmptyDataViews(toShow)
        }
    }

    private fun setupAdapter() {
        binding.recyclerView.layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
        binding.recyclerView.adapter = adapter
        todoViewModel.getAllData.observe(viewLifecycleOwner) { todoList ->
            shareViewModel.checkIfDatabaseEmpty(todoList)
            adapter.setData(todoList)
            this.todoList = todoList
            binding.recyclerView.scheduleLayoutAnimation()
        }
        //configura o swipe to delete da recyclerview

        swipeToDelete(binding.recyclerView)
    }

    private fun swipeToDelete(recyclerView: RecyclerView){
        val swipeToDeleteCallback = object  : SwipeToDelete() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val deletedItem = adapter.getAdapterItemPosition(viewHolder)
                //remove item
                todoViewModel.deleteItem(deletedItem)
                adapter.notifyItemRemoved(viewHolder.adapterPosition)

                //desfaz a delecao do item
                restoreDeletedData(viewHolder.itemView,deletedItem,viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_delete_all -> deleteAll()
            R.id.menu_priority_high -> sortByHighPriority()
            R.id.menu_priority_low -> sortByLowPriority()
        }
        return super.onOptionsItemSelected(item)
    }



    private fun sortByLowPriority() {
        todoViewModel.sortByLowPriority.observe(viewLifecycleOwner){ lowPriorityList ->
            adapter.setData(lowPriorityList)
        }
    }

    private fun sortByHighPriority() {
        todoViewModel.sortByHighPriority.observe(viewLifecycleOwner){ highPriorityList ->
            adapter.setData(highPriorityList)
        }
    }

    private fun deleteAll() {
        val alertDialogBuilder = AlertDialog.Builder(requireContext())
        alertDialogBuilder.setTitle(getString(R.string.delete_all_tasks))
        alertDialogBuilder.setMessage(getString(R.string.delete_all_tasks_body_alert))

        alertDialogBuilder.setPositiveButton(getString(R.string.yes_confirm)){ _,_ ->
            todoViewModel.deleteAll()
            Toast.makeText(requireContext(),getString(R.string.delete_all_successfully), Toast.LENGTH_SHORT).show()
        }

        alertDialogBuilder.setNegativeButton(getString(R.string.no_confirm)){ _,_ -> }
        alertDialogBuilder.create().show()
    }


    private fun restoreDeletedData(view: View,deletedItem: ToDoData,position: Int){
        val msg = (getString(R.string.deleted) + " ${deletedItem.title}")
        val snackBar = Snackbar.make(
            view,
            msg,
            Snackbar.LENGTH_LONG
        )
        snackBar.setAction(getString(R.string.undo_delete)){
            todoViewModel.insertData(deletedItem)
        }
        snackBar.show()
    }


    private fun showEmptyDataViews(toShow: Boolean = true) {
        if(toShow){
            binding.noDataImageview.visibility = View.VISIBLE
            binding.noDataTextView.visibility = View.VISIBLE
        }else{
            binding.noDataImageview.visibility = View.GONE
            binding.noDataTextView.visibility = View.GONE
        }
    }


    override fun onQueryTextSubmit(query: String?): Boolean {
        if(query?.isNotEmpty() == true){
            searchThroughDatabase(query)
        }
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        if(query?.isNotEmpty() == true){
            searchThroughDatabase(query)
        }
        return true
    }


    private fun searchThroughDatabase(query: String) {
        var searchQuery = query
        searchQuery = "%$searchQuery%"
        todoViewModel.searchDatabase(searchQuery).observeOnce(viewLifecycleOwner){ list ->
            list?.let {
                adapter.setData(it)
            }

        }
    }
}