package br.com.todoapp.fragments

class Constants {

    companion object {
        const val HIGH_PRIORITY = "Alta prioridade"
        const val MEDIUM_PRIORITY = "Média prioridade"
        const val LOW_PRIORITY = "Baixa prioridade"
    }

}