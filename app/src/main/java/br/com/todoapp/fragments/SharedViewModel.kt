package br.com.todoapp.fragments

import android.app.Application
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import br.com.todoapp.R
import br.com.todoapp.data.models.Priority
import br.com.todoapp.data.models.ToDoData
import br.com.todoapp.fragments.Constants.Companion.HIGH_PRIORITY
import br.com.todoapp.fragments.Constants.Companion.LOW_PRIORITY
import br.com.todoapp.fragments.Constants.Companion.MEDIUM_PRIORITY

class SharedViewModel(application: Application): AndroidViewModel(application) {

    val emptyDatabase: MutableLiveData<Boolean> = MutableLiveData(true)

    fun checkIfDatabaseEmpty(toDoData: List<ToDoData>){
        emptyDatabase.value = toDoData.isEmpty()
    }


    //altera a cor do spinner de prioridades
    val spinnerListener: AdapterView.OnItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            when(position){
                0 -> { (parent?.getChildAt(0) as TextView).setTextColor(ContextCompat.getColor(application, R.color.red)) }
                1 -> { (parent?.getChildAt(0) as TextView).setTextColor(ContextCompat.getColor(application, R.color.yellow)) }
                2 -> { (parent?.getChildAt(0) as TextView).setTextColor(ContextCompat.getColor(application, R.color.green)) }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {} //sem acao
    }

    fun verifyDataFromUser(title: String,description: String): Boolean {
        return title.isNotBlank() && description.isNotBlank()

    }

    fun parsePriority(priority: String): Priority {
        return when(priority) {
            HIGH_PRIORITY -> Priority.HIGH
            MEDIUM_PRIORITY -> Priority.MEDIUM
            LOW_PRIORITY -> Priority.LOW
            else -> Priority.LOW
        }
    }

    fun parsePriority(priority: Priority): Int{
        return when(priority){
            Priority.HIGH -> 0
            Priority.MEDIUM -> 1
            Priority.LOW -> 2
        }
    }

}