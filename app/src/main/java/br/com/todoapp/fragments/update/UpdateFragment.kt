package br.com.todoapp.fragments.update

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.todoapp.R
import br.com.todoapp.data.models.ToDoData
import br.com.todoapp.data.viewmodel.ToDoViewModel
import br.com.todoapp.databinding.FragmentUpdateBinding
import br.com.todoapp.fragments.SharedViewModel


class UpdateFragment : Fragment() {

    private var _binding: FragmentUpdateBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<UpdateFragmentArgs>()
    private lateinit var toDoData: ToDoData
    private val sharedViewModel: SharedViewModel by viewModels()
    private val toDoViewMode: ToDoViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUpdateBinding.inflate(layoutInflater)
        setHasOptionsMenu(true)
        toDoData = args.currentItem
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.update_fragment_menu,menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun setupViews() {
        binding.todoNameEdit.setText(toDoData.title)
        binding.currentTodoDescriptionEdit.setText(toDoData.description)
        binding.spinnerPriority.setSelection(sharedViewModel.parsePriority(toDoData.priority))
        binding.spinnerPriority.onItemSelectedListener = sharedViewModel.spinnerListener

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_save -> updateItem()
            R.id.menu_delete -> deleteItem()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateItem() {
        val title = binding.todoNameEdit.text.toString()
        val description = binding.currentTodoDescriptionEdit.text.toString()
        val priority = binding.spinnerPriority.selectedItem.toString()
        val validation = sharedViewModel.verifyDataFromUser(title,description)
        if(validation) {
            val updateItem = ToDoData(
                id = toDoData.id,
                title = title,
                description = description,
                priority = sharedViewModel.parsePriority(priority)
            )
            toDoViewMode.updateData(updateItem)
            Toast.makeText(requireContext(),getString(R.string.updated_task),Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(),getString(R.string.fail_update_task),Toast.LENGTH_SHORT).show()
        }
    }

    private fun deleteItem(){
        val alertDialogBuilder = AlertDialog.Builder(requireContext())

        alertDialogBuilder.setPositiveButton(getString(R.string.yes_confirm)){ _,_ ->
            toDoViewMode.deleteItem(toDoData)
            Toast.makeText(requireContext(),getString(R.string.removed_item_successfully),Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }

        alertDialogBuilder.setNegativeButton(getString(R.string.no_confirm)){ _,_ -> }

        alertDialogBuilder.setTitle(getString(R.string.delete_item_title) + " ${toDoData.title} ?")
        alertDialogBuilder.setMessage(getString(R.string.delete_item_body_alert))
        alertDialogBuilder.create().show()
    }
}